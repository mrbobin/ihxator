# frozen_string_literal: true

module Ihxator
  class Monitor
    SUCCESS_STATUSES = [200..399, [401, 403, 404, 422, 429]]

    attr_reader :containers, :logger, :queue
    attr_accessor :proxy_restarted_at

    def initialize(logger: Logger.new($stdout))
      @containers = Set.new
      @logger = logger
    end

    def execute(warmup: 0, wait: 1, refresh: 60)
      sleep warmup

      loop do
        load_containers
        logger.debug("Monitoring #{containers.size} containers")

        refresh.times do
          check_containers

          sleep wait
        end
      end
    end

    private

    def load_containers
      logger.debug("Loading all container")

      each_container("ihxator.enable=true") do |container|
        containers << Container.new(container, logger: logger)
      end
    end

    def check_containers
      containers.each { |container| container.async.check! }
      restart_proxy if containers.any?(&:unhealthy?)
    end

    def restart_proxy
      return unless can_restart_proxy?

      self.proxy_restarted_at = Time.current
      each_container("org.opencontainers.image.title=Traefik") do |container|
        Container.new(container, logger: logger).restart
      end
    end

    def each_container(labels, &block)
      Docker::Container
        .all(filters: {label: Array(labels)}.to_json)
        .each(&block)
    end

    def can_restart_proxy?
      return true unless proxy_restarted_at

      proxy_restarted_at + 15.minutes < Time.current
    end
  end
end
