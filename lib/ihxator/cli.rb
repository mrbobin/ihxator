# frozen_string_literal: true

require "thor"

module Ihxator
  class Cli < Thor
    def self.exit_on_failure?
      true
    end

    desc "monitor", "Execute monitoring task"
    option :warmup, aliases: "-s", type: :numeric, desc: "Amount of time to wait before monitoring", default: 60
    option :wait, aliases: "-w", type: :numeric, desc: "Amount of time to wait between iterations", default: 1
    option :refresh, aliases: "-r", type: :numeric, desc: "Amount of time to wait between loading the containers", default: 60
    option :verbose, type: :boolean, aliases: "-v", desc: "Detailed logging"
    def monitor
      $stdout.sync = true
      logger = Logger.new($stdout)
      logger.level = Logger::INFO
      logger.level = Logger::DEBUG if options[:verbose]
      logger.info("Starting monitoring...")

      Ihxator::Monitor
        .new(logger: logger)
        .execute(warmup: options[:warmup], wait: options[:wait], refresh: options[:refresh])
    end
  end
end
