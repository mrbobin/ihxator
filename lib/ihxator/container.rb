# frozen_string_literal: true

module Ihxator
  class Container
    include Concurrent::Async

    SUCCESS_STATUSES = [200..399, [401, 403, 404, 422, 429]]
    MAX_FAILED_CHECKS = 15
    UNHEALTHY_FAILED_CHECKS = 10

    attr_accessor :checked_at, :failed_checks, :restarted_at
    attr_reader :raw_container, :logger

    def initialize(raw_container, logger: Logger.new($stdout))
      @raw_container = raw_container
      @failed_checks = 0
      @logger = logger
      touch!
    end

    def check!
      return unless can_check?
      touch!

      if service_available?
        self.failed_checks = 0
      else
        logger.info("Restarting container #{human_id}")
        self.failed_checks += 1
        restart
      end
    end

    def restart
      raw_container.restart
      self.restarted_at = Time.current
    rescue
      false
    end

    def unhealthy?
      failed_checks > UNHEALTHY_FAILED_CHECKS
    end

    def id
      raw_container.id
    end

    def hash
      id.hash
    end

    def eql?(other)
      id == other.id
    end

    private

    def human_id
      @human_id ||= labels
        .values_at("com.docker.compose.project", "com.docker.compose.service")
        .join(":")
    end

    def service
      @service ||= labels["ihxator.service"]
    end

    def interval
      @interval ||= labels.fetch("ihxator.interval", 30).to_i
    end

    def labels
      @labels ||= raw_container.info.dig("Labels")
    end

    def touch!
      self.checked_at = current_time
    end

    def service_available?
      status = Excon.get(service, read_timeout: 5, connect_timeout: 5).status
      logger.debug("#{human_id} responded with #{status}")
      SUCCESS_STATUSES.any? { |statuses| statuses.include?(status) }
    rescue => e
      logger.debug("#{human_id} failed with #{e.message}")
      false
    end

    def can_check?
      return false if failed_checks > MAX_FAILED_CHECKS

      checked_at + interval < current_time
    end

    def current_time
      Time.current
    end
  end
end
