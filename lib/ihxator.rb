# frozen_string_literal: true

require "bundler/setup"
Bundler.require(:default)

require "logger"
require "json"
require "active_support/all"
require "docker-api"

require_relative "ihxator/version"
require_relative "ihxator/cli"
require_relative "ihxator/container"
require_relative "ihxator/monitor"

module Ihxator
  class Error < StandardError; end
end
