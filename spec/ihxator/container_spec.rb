# frozen_string_literal: true

RSpec.describe Ihxator::Container do
  let(:labels) { {} }
  let(:raw_container) do
    instance_double(Docker::Container, id: "container-id", info: {"Labels" => labels})
  end

  subject(:container) { described_class.new(raw_container) }

  describe "#id" do
    it "returns the raw container id" do
      expect(container.id).to eq("container-id")
    end
  end

  describe "#eql?" do
    it "returns true when they have the same id" do
      other = described_class.new(instance_double(Docker::Container, id: "container-id"))

      expect(container).to be_eql(other)
    end

    it "returns false for different ids" do
      other = described_class.new(instance_double(Docker::Container, id: "other-container-id"))

      expect(container).not_to be_eql(other)
    end
  end

  describe "#hash" do
    it "returns true when they have the same id" do
      other = described_class.new(instance_double(Docker::Container, id: "container-id"))

      expect(container.hash).to eq(other.hash)
    end

    it "returns false for different ids" do
      other = described_class.new(instance_double(Docker::Container, id: "other-container-id"))

      expect(container.hash).not_to eq(other.hash)
    end
  end

  describe "#unhealthy?" do
    it { expect(container.unhealthy?).to be_falsey }

    context "when the container was restarted too many times" do
      before do
        expect(container)
          .to receive(:failed_checks)
          .and_return(described_class::UNHEALTHY_FAILED_CHECKS + 1)
      end

      it { expect(container.unhealthy?).to be_truthy }
    end
  end

  describe "#restart" do
    it "delegates to the raw container and changes the timestamp" do
      expect(raw_container).to receive(:restart)

      expect { container.restart }.to change { container.restarted_at }
    end
  end

  describe "#check!" do
    before do
      container.checked_at = 1.minute.ago
    end

    context "when the container was restarted too many times" do
      before do
        expect(container)
          .to receive(:failed_checks)
          .and_return(described_class::MAX_FAILED_CHECKS + 1)
      end

      it "does not call restart" do
        expect(container).not_to receive(:restart)

        container.check!
      end
    end

    it "restarts once per interval" do
      expect(container).to receive(:service_available?).once.and_return(false)
      expect(container).to receive(:restart).once

      2.times { container.check! }
    end

    it "changes the check timestamp" do
      expect(container).to receive(:restart)

      expect { container.check! }.to change { container.checked_at }
    end

    it "resets the failure counts when the container is reachable" do
      container.failed_checks = 5
      expect(container).to receive(:service_available?).and_return(true)

      expect { container.check! }.to change { container.failed_checks }.to(0)
    end

    it "increases the failure counts when the container is unreachable" do
      expect(container).to receive(:service_available?).and_return(false)
      expect(container).to receive(:restart).once

      expect { container.check! }.to change { container.failed_checks }.by(1)
    end

    context "when custom interval is larger than the period" do
      let(:labels) do
        {"ihxator.interval" => 2.minutes}
      end

      it { expect { container.check! }.not_to change { container.checked_at } }
    end

    context "when custom interval is smaller than the period" do
      before do
        allow(container).to receive(:restart)
      end

      let(:labels) { {"ihxator.interval" => 15.seconds} }

      it { expect { container.check! }.to change { container.checked_at } }
    end

    context "with service label" do
      let(:service) { "https://example.com" }
      let(:labels) { {"ihxator.service" => service} }

      before do
        response = double
        expect(Excon).to receive(:get).with(service, anything).and_return(response)
        expect(response).to receive(:status).and_return(status)
      end

      context "when the service returns 200" do
        let(:status) { 200 }

        it "does not restart the container" do
          expect(container).not_to receive(:restart)

          container.check!
        end
      end

      context "when the service returns 500" do
        let(:status) { 500 }

        it "restarts the container" do
          expect(container).to receive(:restart)

          container.check!
        end
      end
    end
  end
end
