FROM ruby:alpine

WORKDIR /ihxator

COPY . /ihxator

RUN bundle install
ENTRYPOINT ["bin/ihxator"]
CMD ["monitor"]

