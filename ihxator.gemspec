# frozen_string_literal: true

require_relative "lib/ihxator/version"

Gem::Specification.new do |spec|
  spec.name = "ihxator"
  spec.version = Ihxator::VERSION

  spec.authors = ["Marius Bobin"]
  spec.email = ["marius@mbobin.me"]

  spec.summary = "Simple docker monitoring solution"
  spec.homepage = "https://mbobin.me/ihxator"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 3.0.0"

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://mbobin.me/ihxator"

  spec.files = Dir["lib/**/*", "LICENSE.txt", "README.md"]
  spec.executables = %w[ihxator]

  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "activesupport", "~> 7.0", ">= 7.0.4.2"
  spec.add_runtime_dependency "thor", "~> 1.2", ">= 1.2.1"
  spec.add_runtime_dependency "docker-api", "~> 2.2"
  spec.add_runtime_dependency "concurrent-ruby", "~> 1.2", ">= 1.2.2"

  spec.add_development_dependency "rake", "~> 13.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "standard", "~> 1.3"
end
